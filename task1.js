function compute(x, y) {
    if (x < y) {
        return x + y;
    } else if (x > y) {
        return x - y;
    } else {
        return x * y;
    }
}

console.log(compute(10,20));
console.log(compute(20,10));
console.log(compute(10,10));
