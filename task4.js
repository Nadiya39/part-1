function doubleFactorial(n) {
    if (n === 0 || n === 1) {
        return 1;
    }   
    return n * doubleFactorial(n - 2);
}

console.log(doubleFactorial(0));
console.log(doubleFactorial(2));
console.log(doubleFactorial(9));
console.log(doubleFactorial(14));