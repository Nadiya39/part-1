function sumArrays(array1, array2) {
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    sum1 = array1.reduce(reducer);
    sum2 = array2.reduce(reducer);
    return sum1 + sum2;
}

let a = [1, 2, 3, -5, 0, 10];
let b = [5, -1, 7];
let result = sumArrays(a, b);
console.log('sumArrays ', result);
