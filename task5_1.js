function compareAge(person1, person2) {
    let comparingString = "the same age as";

    if (person1.age > person2.age) {
        comparingString = "older";
    }

    if (person1.age < person2.age) {
        comparingString = "younger";
    }
    
    return person1.name + " is " + comparingString + " " + person2.name;
}

person1 = {name: "Vasyl", age: 20}
person2 = {name: "Petro", age: 16}
person3 = {name: "Marina", age: 20}
console.log(compareAge(person1, person2));
console.log(compareAge(person2, person1));
console.log(compareAge(person1, person3));