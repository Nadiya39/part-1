const persons = [
    { name: "Vasyl", age: 20},
    { name: "Petro", age: 23},
    { name: "Olena", age: 21},
]

function sortByAge(person1, person2) {
    if (person1.age > person2.age) {
        return 1;
    } 
    if (person1.age < person2.age) {
        return -1;
    }
    return 0;
}

persons.sort(sortByAge)
console.log('sorted asc =', persons)
persons.reverse()
console.log('sorted desc =', persons);

